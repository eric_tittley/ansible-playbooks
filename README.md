# Ansible playbooks for The IfA/WFAU clusters #

[Ansible](www.ansible.com) is a cluster-management infrastructure in which tasks are controlled by [Playbooks](http://docs.ansible.com/ansible/playbooks_intro.html).

This repository allows us to

 * share playbooks used on the various systems
 * rationalize playbooks for common tasks so we use the same playbooks for the same tasks on the various systems.

Playbooks are run with:

```
#!bash
ansible-playbook --become --ask-become-pass  playbook.yml

```