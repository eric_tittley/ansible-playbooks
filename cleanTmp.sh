#!/bin/bash
# Clean up Ansible's mess in the /tmp's.  This issue will likely go away in a
# future version of Ansible.
# 
# Run it ny the last user of Ansible.

rm -fr /tmp/.ansible

for i in `cat /usr/local/etc/workerlist`
do
 ssh $i "rm -fr /tmp/ansible" &
done

for i in `cat /usr/local/etc/fcfslist`
do
 ssh $i "rm -fr /tmp/ansible" &
done
